package com.example.hadoopdemo.executor.recommend.movie;

import org.apache.hadoop.mapred.JobConf;

import java.util.regex.Pattern;

/**
 * @author Ruison
 * @date 2021/12/7
 */
public class Recommend {
    public static final Pattern DELIMITER = Pattern.compile("[\t,]");

    public static JobConf config() {
        JobConf conf = new JobConf(Recommend.class);
        conf.setJobName("Recommend");
        conf.addResource("D:\\Project\\Plugin\\hadoop-3.1.2\\etc\\hadoop\\core-site.xml");
        conf.addResource("D:\\Project\\Plugin\\hadoop-3.1.2\\etc\\hadoop\\hdfs-site.xml");
        conf.addResource("D:\\Project\\Plugin\\hadoop-3.1.2\\etc\\hadoop\\mapred-site.xml");
        return conf;
    }
}
